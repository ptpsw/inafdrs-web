<header>
    <div class="navbar">
        <a href="index.php" style="color: black;">InaFDRS</a>
        <div class="nav-right">
            <div class="dropdown">
                <button class="dropbtn">Peta
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content">
                    <a href="/cuaca.php">Cuaca</a>
                    <a href="/human-caused.php">Human Caused</a>
                    <a href="/valuasi.php">Valuasi</a>
                    <a href="/fuels.php">Fuel</a>
                </div>
            </div>
            <a href="/data.php">Data</a>
            <a href="/simulasi.php">Simulasi</a>
            <a href="/hitung-kerugian.php">Perhitungan Kerugian</a>
            <div class="dropdown">
                <button class="dropbtn">Tentang
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content">
                    <a href="/sejarah.php">Sejarah</a>
                    <a href="/sekilas.php">Sekilas</a>
                    <a href="/lisensi.php">Ketentuan</a>
                    <a href="/background-teknis.php">Latar Belakang Teknis</a>
                </div>
            </div>
            <a href="/kontak.php">Kontak</a>
        </div>
    </div>
</header>