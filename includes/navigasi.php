
<table width="100%" border="0" cellspacing="0" cellpadding="3" style="height:100%">
  <tr>
    <td>
      <div class="navbar">
        <a href="index.php">Dashboard</a>

        <div class="dropdown">
          <button class="dropbtn">Data
            <i class="fa fa-caret-down"></i>
          </button>
          <div class="dropdown-content">
            <a href="/cuaca.php">Cuaca</a>
            <a href="/human-caused.php">Human Caused</a>
            <a href="/valuasi.php">Valuasi</a>
            <a href="/fuels.php">Fuel</a>
            <a href="/lain-lain.php">Lainnya</a>
          </div>
        </div>
        <a href="/ketersediaan-data">Ketersediaan Data</a>
        <a href="/simulasi">Simulasi</a>
        <a href="/hitung-kerugian">Perhitungan Kerugian</a>
        <a href="/pengaturan">Pengaturan</a>
        <a href="/tentang">Tentang Kami</a>
      </div>
    </td>
  </tr>
</table>
