<div id="drawer" class="hide-drawer">
    <div>
        <a href="index.php">Dashboard</a>
    </div>
    <div class="">
        <span class="">Data <i class="fa fa-caret-down"></i></span>
        <div class="">
            <a href="/cuaca.php">Cuaca</a>
            <a href="/human-caused.php">Human Caused</a>
            <a href="/valuasi.php">Valuasi</a>
            <a href="/fuels.php">Fuel</a>
            <a href="/lain-lain.php">Lainnya</a>
        </div>
    </div>
    <div>
        <a href="/ketersediaan-data">Ketersediaan Data</a>
    </div>
    <div>
        <a href="/simulasi">Simulasi</a>
    </div>
    <div>
        <a href="/hitung-kerugian">Perhitungan Kerugian</a>
    </div>
    <div>
        <a href="/pengaturan">Pengaturan</a>
    </div>
    <div>
        <a href="/tentang">Tentang Kami</a>
    </div>
</div>