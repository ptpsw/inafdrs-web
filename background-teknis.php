<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head-tag-contents.php") ?>
</head>
<body>
    <div class="container">
        <!-- header nav -->
        <?php include("includes/header.php") ?>

        <!-- side drawer -->
        <?php include("includes/drawer.php") ?>

        <!-- content -->
        <div class="article">
            <h1>Tentang Kita</h1>
            <p>Pada periode tahun 1997/1998 bencana kebakaran Indonesia mengalami kondisi yang sangat parah. Sehingga di akhir tahun 1990an pemerintah Indonesia melalui BPPT (Badan Pengkajian dan Penerapan Teknologi) bekerjasama dengan pemerintah Canada (dalam hal ini Canadian Forest Service) membangun sistem peringatan dini berupa Sistem Pemeringkat Bahaya Kebakaran (SPBK, atau Fire Danger Rating Sistem). BPPT Bersama dengan Departemen Kehutanan dan Badan Meteorologi dan Geofisika mengadopsi FDRS dari Canada ini untuk diterapkan di Indonesia. Badan Meteorologi dan Geofisika (sekarang BMKG) kemudian menjadi institusi yang mengoperasikan sistem ini hingga saat ini (Guswanto, 2009)</p>

            <p>Pengembangan dari FDRS yang telah diterapkan di Indonesia selama ini dengan algoritma pengembangan untuk menjadi Ina-FDRS, yang lebih fokus pada skala lokal dan spesifik pada lahan gambut. Ina-FDRS adalah pengembangan dari FDRS yang telah ada dimana ditambahkan tiga komponen utama dalam perhitungannya yaitu komponen yang disebabkan oleh aktivitas manusia (human-caused), komponen bahan bakaran yang ada di lokasi pada permukaan ataupun bawah permukaan (fuels) dan juga komponen potensi kerugian dari hitungan akuntansi sumber daya alam (NRA). </p>
            <img src="image/inafdrs-diagram.png" alt="Ina-FDRS Diagram">

            <p>Kabupaten Ogan Komering Ilir di Provinsi Sumatera Selatan adalah lokasi yang dipilih sebagai daerah studi. Lahan gambut yang luas dan peristiwa kebakaran hutan dan lahan yang hebat pada tahun 2015 menjadi alasan utama pemilihan lokasi ini.</p>

            <h1>Technical Background</h1>
            <h2>Fire Weather Index (FWI)</h2>
            <p>Merupakan peringkat numerik dari intensitas kebakaran. Indek ini secara umum dapat disebut sebagai indek bahaya kebakaran ditinjau dari segi cuaca (curah hujan harian, kelembaban relative, suhu dan kecepatan angin). Bahaya kebakaran adalah indikasi umum dari semua faktor yang mempengaruhi kemudahan terbakar, penyebaran api, dampak fisik kebakaran dan tingkat kesulitan pengendalian kebakaran. Kode ini digunakan sebagai indikator prakiraan kesulitan pengendalian kebakaran.</p>
            <!-- <p>{{tabel legend FWI}}</p> -->

            <p>Parameter cuaca yang menjadi masukan system dalam komponen FWI yaitu FFMC, DMC, DC, dan ISI. FFMC (Fine Fuel Moisture Code) merupakan peringkat numerik dari kandungan kelembaban dari serasah dan bahan bakar halus lainnya. Kode ini menandakan kemudahan relatif mulainya api dan terbakarnya. Kode ini berkorelasi dengan kejadian- kejadian kebakaran yang disebabkan manusia. Kode ini digunakan untuk indikator potensi penyulutan api menjadi kebakaran.</p>

            <p>DMC (Duff Moisture Code) merupakan peringkat numerik dari kelembaban rata- rata dari lapisan tanah organik yang tidak padat dengan kedalaman sedang. Kode ini memberikan indikasi konsumsi bahan bakar pada lapisan humus sedang dan materi berkayu berukuran sedang. DC (Drought Code) merupakan peringkat numerik dari kandungan kelembaban dari lapisan tanah organik yang padat. Kode ini adalah indikator penting dari dampak kemarau musiman pada bahan bakar hutan, dan banyaknya nyala bara api dalam lapisan organik yang dalam dan bongkahan kayu besar. Kode ini digunakan sebagai indikator potensi membaranya api dalam suatu kebakaran dan potensi terjadinya kabut asap.</p>

            <h2>Human Caused</h2>
            <p>Merupakan komponen yang memperhitungkan aktivitas manusia yang dapat mempengaruhi pembakaran hutan, kebun, dan lahan. Aktivitas manusia yang menjadi masukan dalam komponen pengaruh pembakaran antara lain jaringan jalan, jaringan sungai, serta jarak terhadap pemukiman. Pembangunan analisa spasial menggunakan peta acuan dari Peta Rupa Bumi Indonesia dengan sumber data BIG tahun 2015 skala 1:50.000 ataupun survey lapangan. Jejaring sungai, jalan dan jarak terhadap pemukiman menghasilkan peta angka dari metode map distance.</p>
            <h2>Fuel (komponen bahan bakaran)</h2>
            <p>Merupakan bahan bakaran yang memperhitungkan materi di permukaan, seperti data sebaran ketebalan, distribusi lahan gambut, dan penutupan lahan. Pada ketebalan gambut maka gambut yang mempunyai ketebalan tinggi akan mempunyai kerentanan yang tinggi pula dibandingkan ketebalan yang rendah. Hal ini terkait dengan tersedianya bahan bakaran baik gambut yang dipermukaan maupun yang dikedalaman. Sebaran ketebalan gambut mempunyai ketebalan berkisar 1 hingga 10 meter. Sedangkan kelas penutupan lahan terdiri dari: daerah terbangun, tubuh air, vegetasi padat, vegetasi menengah, dan vegetasi jarang. Klasifikasi penutupan lahan dihasilkan melalui proses machine learning dengan metode random forest pada data satelit Landsat 8 OLI tahun 2017. Pembobotan variabel komponen bahan bakaran dilakukan dengan expert judgment dengan metode AHP (Analyical Hiearchy Process).</p>
            <h2>Economic Value</h2>
            <p>Merupakan penilaian suatu area dari kerusakan sumberdaya yang akan menimbulkan kerugian dengan pendekatan nilai total ekonomi yang hilang akibat kerusakan yang terjadi dan timbulnya biaya akibat dampak kebakaran lahan gambut. Cara menghitung valuasi ekonomi  kerugian akibat kebakaran lahan pada ekosistem gambut, perlu diketahui fungsi dan manfaat sumberdaya alam di ekosistem gambut maupun sumberdaya Alam di atasnya yang terganggu atau mengalami perubahan bahkan hilang yang menjadi fokus perhitungan sesuai dengan tujuan valuasi ekonomi. Berdasarkan Peraturan Menteri Negara Lingkungan Hidup Republik Indonesia No.14 Tahun 2012 tentang Panduan Valuasi Ekonomi Ekosistem Gambut, ada 19 fungsi dan manfaat dari ekosistem Gambut yang terdiri dari; 1) Sumber bahan energi, 2) Media Tanam/Pupuk organic), 3) Lahan untuk HTI (Hutan Tanaman Industri), 4) Perkebunan, 5) Pertanian, 6) Ekowisata,7) Pendidikan, 8) Penelitian, 9) Penambat/penyimpan air, 10) Pencegah banjir/kabakaran, 11) Penyerap karbon, 12) Penyimpan karbon, 13) Penghasil Oksigen, 14) Tempat perkembangbiakan hewan, 15) Flora/Fauna , 16) Aktivitas spiritual keagamaan, 17) Penyedia jasa transportasi, 18) Cagar alam, 19) Estetika , dan apabila ekositem lahan gambut mengalami kerusakan akibat ulah manusia, misal lahan gambut dibakar untuk pembukaan lahan perkebunan sehingga mengalami kebakaran hebat di lahan gambut maka indikator perhitungan tambah dengan kerugian akibat kebakaran lahan gambut yaitu, 1) Banjir, 2) Dampak akibat kebakaran lahan gambut, yaitu dampak langsung seperti perumahan penduduk dan fasilitas umum dan dampak tidak langsung seperti asap, perubahan cuaca mikro dan perubahan kualitas udara.</p>


        </div>
        <!-- <div class="mapid"></div> -->
    </div>

    <!-- bottom js file -->
    <!-- <script type="text/javascript" src="static/js/map.js"></script> -->
    
</body>
</html>