<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head-tag-contents.php") ?>
  <script type="text/javascript" src="data/ina_fdrs_260418.js"></script>
</head>
<body>
    <div class="container">
        <!-- header nav -->
        <?php include("includes/header.php") ?>

        <!-- side drawer -->
        <?php include("includes/drawer.php") ?>

        <!-- content -->
        <div id="mapid"></div>
    </div>

    <!-- bottom js file -->
    <script type="text/javascript" src="static/js/map.js"></script>
    <script>
        mapBasic.weatherMapInit();
    </script>
</body>
</html>