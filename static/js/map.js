/* 
* Javascript functions mainly for calling map
*/

var mapBasic = {
    defaultLocation : [-3.357629, 105.409458],
    defaultZoom : 9,
    map : {},
    init: function () {
        baseMap = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 13,
            id: 'mapbox.streets',
            accessToken: mapboxToken,
        });

        this.map = L.map('mapid', {
            layers: baseMap 
        }).setView(this.defaultLocation, this.defaultZoom);
    },

    // Utility functions for styling GeoJSON
    getColor: function (d) {
        return d > 1.5 ? '#800026' :
                d > 1  ? '#E31A1C' :
                d > 0.5   ? '#FD8D3C' :
                            '#FFEDA0';
    },
    geojsonMarkerOptions: function (feature) {
        return {
            radius: 8,
            fillColor: this.getColor(feature.properties.Ina_FDRS),
            color: "#000",
            weight: 1,
            opacity: 1,
            fillOpacity: 0.5
        };
    },

    // Dashboard
    dashboardMapInit: function () {
        var fdrsLayer = this.renderFDRS(mapData).addTo(this.map);
    },

    renderFDRS: function (mapData) {
        return L.geoJSON(mapData, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, mapBasic.geojsonMarkerOptions(feature));
            }
        });
    },

    // Weather Map
    weatherMapInit: function () {
        // Layers Render
        var himawariLayer = L.geoJSON();
        var tempLayer = L.geoJSON();
        var humidLayer = L.geoJSON();
        var windLayer = L.geoJSON();
        var ffmcLayer = L.geoJSON();
        var fwiLayer = L.geoJSON();
        var dcLayer = L.geoJSON();
        var buiLayer = L.geoJSON();
        var dmcLayer = L.geoJSON();
        var isiLayer = L.geoJSON();
        var hthLayer = L.geoJSON();

        var baseMaps = {
            "Base": baseMap
        };
        var overlayMaps = {
            "Himawari": himawariLayer,
            "Temperatur": tempLayer,
            "Humidity": humidLayer,
            "Windspeed": windLayer,
            "FFMC": ffmcLayer,
            "FWI": fwiLayer,
            "DC": dcLayer,
            "BUI": buiLayer,
            "DMC": dmcLayer,
            "ISI": isiLayer,
            "HTH": hthLayer
        };

        L.control.layers(baseMaps, overlayMaps).addTo(this.map);
    },

    // Human Caused Map
    hcauseMapInit: function () {
        // Layers Render
        var hutanLayer = L.geoJSON();
        var kebunLayer = L.geoJSON();
        var infraLayer = L.geoJSON();
        var mukimLayer = L.geoJSON();
        var gambutLayer = L.geoJSON();
        var firespotLayer = L.geoJSON();
        var hotspotLayer = L.geoJSON();
        var lahanLayer = L.geoJSON();

        var baseMaps = {
            "Base": baseMap
        };
        var overlayMaps = {
            "Hutan": hutanLayer,
            "Kebun": kebunLayer,
            "Infrastruktur Transportasi": infraLayer,
            "Permukiman": mukimLayer,
            "Peta Gambut": gambutLayer,
            "Firespot": firespotLayer,
            "Hotspot": hotspotLayer,
            "Status Lahan": lahanLayer
        };

        L.control.layers(baseMaps, overlayMaps).addTo(this.map);
    },
    renderHTI: function () {
        var customLayer = L.geoJSON(null, {
            style: function(feature) {
                return { color: '#f00' };
            }
        });
        var htiGreyLayer = omnivore.kml('kml/hti.kml', null, customLayer);
        return htiGreyLayer;
    },

    renderSuaka: function () {
        suakaLayer = omnivore.kml('kml/suaka.kml');
        return suakaLayer;
    },

    // Value Map
    valueMapInit: function () {
        // Layers Render
        var tevLayer = L.geoJSON();

        var baseMaps = {
            "Base": baseMap
        };
        var overlayMaps = {
            "Valuasi Kecamatan": tevLayer
        };

        L.control.layers(baseMaps, overlayMaps).addTo(this.map);
    },

    // Fuel Map
    fuelMapInit: function () {
        // Layers Render
        var peatDepthLayer = L.geoJSON();
        var peatThickLayer = L.geoJSON();
        var gwlLayer = L.geoJSON();

        var baseMaps = {
            "Base": baseMap
        };
        var overlayMaps = {
            "Kedalaman Gambut": peatDepthLayer,
            "Ketebalan Gambut": peatThickLayer,
            "Status GWL (<0.4m atau >=0.4m)": gwlLayer
        };

        L.control.layers(baseMaps, overlayMaps).addTo(this.map);
    }
}

mapBasic.init();