<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head-tag-contents.php") ?>
</head>
<body>
    <div class="container">
        <!-- header nav -->
        <?php include("includes/header.php") ?>

        <!-- side drawer -->
        <?php include("includes/drawer.php") ?>

        <!-- content -->
        <div class="article">
            <h1>Sejarah InaFDRS</h1>
            <p>Kebakaran hutan dan lahan yang terjadi setelah peristiwa El Niño (ENSO) pada tahun 1997/1998 di Indonesia seluas 11,7 juta hektar menimbulkan kerugian sebanyak 6,3 milliar dollar (CIFOR, 2003). Di tahun 1998, BPPT beserta institusi terkait (Departemen Kehutanan dan Pemerintah Daerah) melaksanakan operasi pemadaman kebakaran hutan dan lahan dibawah koordinasi Kementerian Kesejahteraan Rakyat. Perjanjian kerjasama antara Indonesia dan Kanada dengan judul “South East Asia Fire danger Rating System(FDRS) – Indonesia Initiative” dilaksanakan pada tahun 1999 hingga 2002. Pihak Indonesia terdiri dari BPPT, BMG, dan Dep. Kehutanan, dan pihak Kanada diwakili oleh Canadian Forest Services. Kegiatan ini terdiri dari 4 bagian, yaitu: Adaptasi, Operasi, Aplikasi , dan sistem asap regional. </p>

            <p>Pengembangan sistem FDRS  dilanjutkan pada tahun 2003 hingga 2005 oleh BPPT, Dep. Kehutanan dan BMKG melalui kerjasama yang berfokus pada (1) operasional FDR, (2) peningkatan kompetensi di Dep. Kehutanan, dan (3) diseminasi produk.  Pembangunan FDRS ini masih berbasis pada parameter lahan gambut di sub tropis (kanada) dengan menggunakan data cuaca harian (kecepatan angin, curah hujan, suhu dan kelembaban relatif), Pada tahun 2004, Dep. Kehutanan telah memproduksi AWS dan perhitungan FWI (Fire weather Index) untuk 29 kawasan di 8 propinsi. Di tahun 2005, LAPAN mengadopsi s-FMS berbasis data penginderaan jauh dan BMKG melaksanakan pengembangan kompetensi menggunakan perhitungan XLFWI di Riau dan Pontianak. Di tahun 2006, BMKG mengeluarkan prediksi FDR untuk 3 harian dan melanjutkan peningkatan kompetensi perhitungan XLFWI untuk propinsi Jambi, Lampung dan Palangka Raya. Di tahun 2007, BMKG mengeluarkan prediksi untuk 7 harian dengan bekerjasama dengan CSIRO. Sejak 2008, BMKG telah memasang dan menjalankan FDRS dan distribusi asap.</p>

            <p>Kebakaran hutan di tahun 2015 seluas 2,6 juta hektar sangat berdampak pada perekonomian Indonesia dimana kerugian sebanyak Rp. 221 triliun (sumber: World Bank dan Bank Indonesia). Pusat Teknologi Pengembangan Sumberdaya Wilayah (PTPSW)-BPPT sejak 2017 melakukan kajian untuk pembaharuan parameter FDRS dengan merapatkan resolusi spasial dan masukan data (iklim dan cuaca (FWI), lahan gambut Indonesia dan penggunaan lahan (Fuel), dan nilai ekonomi lahan gambut (TEV). Di bulan Desember 2018, BPPT meluncurkan Sistem Pemeringkatan Bahaya Kebakaran Lahan Gambut Indonesia atau Indonesia Peatland FDRS (Ina-FDRS) untuk Kabupaten Ogan Komering Ilir, Propinsi Sumatera Selatan.  Sepanjang tahun 2019, PTPSW melakukan validasi sistem ini di Kabupaten Ogan Komering Ilir.</p>
        </div>
        <!-- <div class="mapid"></div> -->
    </div>

    <!-- bottom js file -->
    <!-- <script type="text/javascript" src="static/js/map.js"></script> -->
    
</body>
</html>