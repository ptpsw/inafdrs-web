<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head-tag-contents.php") ?>
</head>
<body>
    <div class="container">
        <!-- header nav -->
        <?php include("includes/header.php") ?>

        <!-- side drawer -->
        <?php include("includes/drawer.php") ?>

        <!-- content -->
        <div class="article">
            <h1>Sekilas InaFDRS</h1>
            <p>Sistem Pemeringkatan Bahaya Kebakaran Lahan Gambut Indonesia  atau Indonesia peatland FDRS (Ina-FDRS) dikembangkan oleh Pusat Teknologi Pengembangan Sumberdaya Wilayah (PTPSW), Badan Pengkajian Penerapan Teknologi (BPPT), untuk wilayah Kabupaten Ogan Komering Ilir, Propinsi Sumatera Selatan. Ina-FDRS merupakan revisi FDRS yang telah dikembangkan sejak tahun 2000-an. Tujuan pengembangan Ina-FDRS adalah untuk memperbaiki keakurasian peringkat kebakaran lahan gambut dengan menggunakan grid berukuran 4x4 km dan memperbaiki komponen-komponen berikut ini: </p>
            <ul>
                <li>menggunakan lahan gambut di Indonesia (gambut tropis) sebagai bahan bakaran (Fuel), yang terdiri dari cakupan lahan gambut dan ketebalan lapisan gambut</li>
                <li>merapatkan komponen iklim dan cuaca untuk masukan FWI (Fire Weather Index)</li>
                <li>menambahkan komponen aktrivitas  manusia (Human caused)</li>
                <li>menambahkan nilai ekonomi lahan gambut (economic value)</li>
            </ul>
            <p>Komponen Fuel terdiri dari tutupan lahan hasil klasifikasi citra penginderaan jauh dan aplikasi georadar; komponen iklim memiliki masukan data cuaca yang near-realtime  dan komponen iklim yang merupakan hasil kajian iklim selama 30 tahun terakhir. Komponen Human caused, adalah komponen baru dari FDRS yang telah ada, dan merupakan komponen penting karena sebagian besar terjadinya kebakaran di wilayah Indonesia adalah akibat perilaku manusia. Komponen economic value adalah komponen baru di FDRS yang merupakan hasil kajian PTPSW tentang nilai ekonomi sumberdaya lahan gambut.</p>
            <p>Ina-FDRS diluncurkan pada bulan Desember 2018 untuk lahan gambut di Kabupaten Ogan Komering Ilir (OKI), Propinsi Sumatera Selatan dan selama tahun 2019 akan di evaluasi dan validasi melalui kerjasama dengan BMKG, Pemda OKI, dan PT Sinar Mas Forestry.</p>
        </div>
    </div>

    <!-- bottom js file -->
    <!-- <script type="text/javascript" src="static/js/map.js"></script> -->
    
</body>
</html>