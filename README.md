# inafdrs-web

Indonesia Fire Danger Rating System or Ina-FDRS project based on CFFDRS.

## Scope

Ina-FDRS will cover Ogan Komering Ilir, Sumatera Selatan Province, Indonesia.

inafdrs-web depend on data processing script to convert weather data from AWS to be calculated as FDRS and other indexes. inafdrs-web assume that those data are processed and provided as GeoJSON or KML to be presented on the web.

## Using Vagrant [Optional]

If you use Vagrant you can run this project by running

```bash
vagrant up
vagrant ssh
```

Then install MySQL and set your root account.

Then change apache config to host  `/vagrant/inafdrs-web/` folder.

## How To Run

This project is using LAMP stack with PHP version 5.6.

Config Files you need to set first:

* admin/mysql/database.php
* static/js/config.js
