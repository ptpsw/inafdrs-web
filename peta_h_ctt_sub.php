<link href="admin/style/style.css" rel="stylesheet" type="text/css">
<link href="admin/style/InaSelect.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript'>
    var map,
        maxPopulation = 22,
        heatGradientData;
    function GetMap() {
        map = new Microsoft.Maps.Map('#myMap', {
            center: new Microsoft.Maps.Location(-3.1972588, 104.6981135),
            zoom: 8
        });
        //Create a legend.
        createLegend(maxPopulation);
        //Load the GeoJSON module.
        Microsoft.Maps.loadModule('Microsoft.Maps.GeoJson', function () {
            //Read the GeoJSON file that is hosted on the same domain.
            Microsoft.Maps.GeoJson.readFromUrl('kml/ctt.json', function (data) {
                //Loop through results and set the fill color of the polygons based on the GRID_CODE property.
                for (var i = 0; i < data.length; i++) {
                    data[i].setOptions({
                        fillColor: getLegendColor(data[i].metadata.GRID_CODE, maxPopulation)
                    });
                    //Add a click event to each polygon and display metadata.
                    Microsoft.Maps.Events.addHandler(data[i], 'click', function (e) {
                        alert('\r\Point ID Nomor : ' +e.target.metadata.POINTID + '\r\Nilai Cloud Top Temperature : ' + e.target.metadata.GRID_CODE);
                    });
                }
                //Add results to the map.
                map.entities.push(data);
            }, null, { polygonOptions: { strokeColor: 'white' } });
        });
    }
    function createLegend(maxValue) {
        var canvas = document.getElementById('legendCanvas');
        var ctx = canvas.getContext('2d');
        //Create a linear gradient for the legend.
        var colorGradient = {
            "0.00": 'rgba(0,255,0,255)',    // Green
            "0.50": 'rgba(255,255,0,255)',  // Yellow
            "1.00": 'rgba(255,0,0,255)'     // Red
        };
        var grd = ctx.createLinearGradient(0, 0, 256, 0);
        for (var c in colorGradient) {
            grd.addColorStop(c, colorGradient[c]);
        }
        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        //Store the pixel information from the legend.
        heatGradientData = ctx.getImageData(0, 0, canvas.width, 1);
    }
    function getLegendColor(value, maxValue) {
        value = (value > maxValue) ? maxValue : value;
        //Calculate the pixel data index from the ratio of value/maxValue.
        var idx = Math.round((value / maxValue) * 256) * 4 - 4;
        if (idx < 0) {
            idx = 0;
        }
        //Create an RGBA color from the pixel data at the calculated index.
        return 'rgba('+ heatGradientData.data[idx]+ ',' +
            heatGradientData.data[idx + 1] + ',' +
            heatGradientData.data[idx + 2] + ',' + '0.5)';
    }
    </script>

    <script type='text/javascript' src='https://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=ArqYFyp4a5yy9-0j9F65lwNyvHyDB3AGLiDJeQkYslt48MiprfoDK3_-R4A7dWZt' async defer></script>


<form action="" method="post" name="formprabu" id="formprabu" style="height:100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="height:100%">
  <tr bgcolor="#F4F4F4">
    <td height="7%">
    <table width="100%" border="0" cellspacing="3s" cellpadding="0" style="height:100%">
  <tr>
    <td width="3%" bgcolor="#090" class="logo_peta_adm">&nbsp;</td>
    <td width="13%" align="right" bgcolor="#D6D6D6" class="border_tbl_atas" onclick="prabuaj();">
    &nbsp;Pilih Tanggal :&nbsp;
    </td>
    <td width="10%">
    <input type="date" name="bday" style="height:100%">
	</td>
    <td width="7%">
    <input type="time" name="bday" style="height:100%">
	</td>
    <td width="8%" bgcolor="#D6D6D6" class="border_tbl_atas" onclick="prabuaj();">
    &nbsp;Tampilkan&nbsp;
    </td>
    <td>
    </td>

</table>

    </td>
  </tr>
  <tr>
    <td bgcolor="#CC9900" id="myMap">&nbsp;</td>
  </tr>
</table>
</form>
<?php 
if(isset($_POST['submit'])){
	$selectboxval = $_POST['pet_adm'];	
}
?>

<script>
        function prabuaj() {
		//var selectedValue = document.getElementById("pet_adm").value;
		//document.getElementById("targetpr").innerHTML = selectedValue;
		//layer.setDataSource(`kml/admin/${selectedValue}.kmz`, true);
		//console.log(selectedValue);
		//alert(selectedValue);					
		}		
</script>

    <style>
        .mapContainer {
            position: relative;
            width: 800px;
            height: 600px;
        }
        #map {
            position: relative;
            width: 800px;
            height: 600px;
        }
        .legend {
            position: absolute;
            top: 120px;
            left: 5px;
            width: 256px;
            height: 35px;
            font-family: Arial;
            font-size: 12px;
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 5px;
            padding: 5px;
        }
        #legendCanvas {
            width: 256px;
            height: 15px;
        }
        .legend-max {
            float: right;
        }
    </style>
        <div class="legend">
            <canvas id="legendCanvas"></canvas>
            <span class="legend-min">Rendah</span>
            <span class="legend-max">Tinggi</span>
        </div>


