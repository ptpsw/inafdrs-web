<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("includes/head-tag-contents.php") ?>
</head>
<body>
    <div class="container">
        <!-- header nav -->
        <?php include("includes/header.php") ?>

        <!-- side drawer -->
        <?php include("includes/drawer.php") ?>

        <!-- content -->
        <div class="article">
            <h1>Ketentuan Penggunaan</h1>
            <p>Ina-FDRS merupakan konsep baru dalam pengembangan FDRS atau sistem 
pemeringkatan bahaya kebakaran hutan pada lahan gambut. Ina-FDRS 
terdiri dari empat komponen utama yaitu Fire Weather Index (FWI), 
Human-Caused, Natural Resources Accounting, dan Fuels. Keempat komponen 
ini di formulasi sehingga menghasilkan sistem pemeringkatan bahaya 
kebakaran hutan yang lebih baik dari sistem FDRS yang telah ada.</p>

            <p>Ina-FDRS ini masih dalam tahap pengembangan dan masih perlu dilakukan 
validasi terhadap data-data yang Fire Weather Index (FWI), Human 
Caused, Natural Resources Accounting, dan Fuels.</p>

        </div>
        <!-- <div class="mapid"></div> -->
    </div>

    <!-- bottom js file -->
    <!-- <script type="text/javascript" src="static/js/map.js"></script> -->
    
</body>
</html>